package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
public class StudentController 
{

	@Autowired
	private StudentRepository repo;
	
	    @GetMapping("/save")
	    public String save()
	    {
	        Student student=new Student();
	        student.setName("vikram shori");
	        repo.save(student);
	    	return "record save";	
	    }
	    
	    @GetMapping("/getAll")
	    public List<Student> getStudent()
	    {
	    	List<Student> list=repo.getStudent();
	    	return list;
	    }
	   
	    @GetMapping("/getName")
	    public List<Student> getStudentByName ()
	    {
	    	List<Student> list=repo.getStudentByName("vikram shori");
	    	return list;
	    }
	    
	    @GetMapping("/getID")
	    public List<Integer> getStudentByID()
	    {
	    	List<Integer> list=repo.getStudentByID("vikram shori");
	    	return list;
	    }
}
