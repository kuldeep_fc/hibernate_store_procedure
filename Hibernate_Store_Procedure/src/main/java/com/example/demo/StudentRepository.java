package com.example.demo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student,Integer>
{

	
	 // @Procedure(value ="getstudent")
	  @Query(value = "getstudent", nativeQuery = true)
	  List<Student> getStudent();

	  
	  @Query(value = "getByName :name", nativeQuery = true)
	  List<Student> getStudentByName(@Param("name")String name);

	  @Query(value = "getByNames :n", nativeQuery = true)
	  List<Integer> getStudentByID(@Param("n")String n);
}
